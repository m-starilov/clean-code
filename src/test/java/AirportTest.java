import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import planes.ExperimentalPlane;
import planes.MilitaryPlane;
import planes.PassengerPlane;
import planes.Plane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static planes.specifications.Role.*;
import static planes.specifications.Secrecy.*;

public class AirportTest {

    private static Airport airport;
    private static PassengerPlane expectedPlaneWithMaxPassengerCapacity;
    private static final List<Plane> planes = new ArrayList<>();
    private static final List<MilitaryPlane> expectedTransportMilitaryPlanes = new ArrayList<>();

    @BeforeTest
    public void setPlanes() {
        planes.add(new PassengerPlane("Boeing-737", 900, 12000, 60500, 164));
        planes.add(new PassengerPlane("Boeing-737", 900, 12000, 60500, 164));
        planes.add(new PassengerPlane("Boeing-737-800", 940, 12300, 63870, 192));
        planes.add(new PassengerPlane("Boeing-747", 980, 16100, 70500, 242));
        planes.add(new PassengerPlane("Airbus A320", 930, 11800, 65500, 188));
        planes.add(new PassengerPlane("Airbus A330", 990, 14800, 80500, 222));
        planes.add(new PassengerPlane("Embraer 190", 870, 8100, 30800, 64));
        planes.add(new PassengerPlane("Sukhoi Superjet 100", 870, 11500, 50500, 140));
        planes.add(new PassengerPlane("Bombardier CS300", 920, 11000, 60700, 196));
        planes.add(new MilitaryPlane("B-1B Lancer", 1050, 21000, 80000, BOMBER));
        planes.add(new MilitaryPlane("B-2 Spirit", 1030, 22000, 70000, BOMBER));
        planes.add(new MilitaryPlane("B-52 Stratofortress", 1000, 20000, 80000, BOMBER));
        planes.add(new MilitaryPlane("F-15", 1500, 12000, 10000, FIGHTER));
        planes.add(new MilitaryPlane("F-22", 1550, 13000, 11000, FIGHTER));
        planes.add(new MilitaryPlane("C-130 Hercules", 650, 5000, 110000, TRANSPORT));
        planes.add(new ExperimentalPlane("Bell X-14", 277, 482, 500, HIGH_ALTITUDE, SECRET));
        planes.add(new ExperimentalPlane("Ryan X-13 Vertijet", 560, 307, 500, VTOL, TOP_SECRET));

        expectedPlaneWithMaxPassengerCapacity = new PassengerPlane("Boeing-747", 980, 16100, 70500, 242);
        expectedTransportMilitaryPlanes.add(new MilitaryPlane("C-130 Hercules", 650, 5000, 110000, TRANSPORT));
    }

    @BeforeMethod
    public void setAirport() {
        airport = new Airport(planes);
    }

    @Test
    public void testGetTransportMilitaryPlanes() {
        List<MilitaryPlane> transportMilitaryPlanes = airport.getTransportMilitaryPlanes();
        assertEquals(expectedTransportMilitaryPlanes, transportMilitaryPlanes);
    }

    @Test
    public void testGetPassengerPlaneWithMaxCapacity() {
        PassengerPlane planeWithMaxPassengersCapacity = airport.getPassengerPlaneWithMaxPassengersCapacity();
        assertEquals(expectedPlaneWithMaxPassengerCapacity, planeWithMaxPassengersCapacity);
    }

    @Test
    public void testGetSortedPlanesByMaxLoadCapacity() {
        planes.sort(Comparator.comparingInt(Plane::getMaxLoadCapacity));
        airport.sortByMaxLoadCapacity();
        assertEquals(planes, airport.getPlanes());
    }

    @Test
    public void testHasAtLeastOneBomberInMilitaryPlanes() {
        List<MilitaryPlane> bomberMilitaryPlanes = airport.getBomberMilitaryPlanes();
        assertTrue(bomberMilitaryPlanes.stream().anyMatch(MilitaryPlane -> MilitaryPlane.getRole() == BOMBER));
    }

    @Test
    public void testExperimentalPlanesHasClassificationLevelHigherThanUnclassified() {
        List<ExperimentalPlane> experimentalPlanes = airport.getExperimentalPlanes();
        assertTrue(experimentalPlanes.stream()
            .allMatch(ExperimentalPlane -> (ExperimentalPlane.getClassificationLevel() != UNCLASSIFIED)));
    }

}
