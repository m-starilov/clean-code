import static planes.specifications.Role.BOMBER;
import static planes.specifications.Role.TRANSPORT;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import planes.ExperimentalPlane;
import planes.MilitaryPlane;
import planes.PassengerPlane;
import planes.Plane;

public class Airport {

  private final List<? extends Plane> planes;

  public Airport(List<? extends Plane> planes) {
    this.planes = planes;
  }

  public List<? extends Plane> getPlanes() {
    return planes;
  }

  public List<ExperimentalPlane> getExperimentalPlanes() {
    return getPlanesOfSpecifiedType(ExperimentalPlane.class);
  }

  public List<MilitaryPlane> getMilitaryPlanes() {
    return getPlanesOfSpecifiedType(MilitaryPlane.class);
  }

  public List<PassengerPlane> getPassengerPlanes() {
    return getPlanesOfSpecifiedType(PassengerPlane.class);
  }

  public PassengerPlane getPassengerPlaneWithMaxPassengersCapacity() {
    return getPassengerPlanes().stream().
        max((Comparator.comparingInt(PassengerPlane::getPassengersCapacity))).get();
  }

  public List<MilitaryPlane> getTransportMilitaryPlanes() {
    return getMilitaryPlanes().stream()
        .filter(plane -> (TRANSPORT == plane.getRole())).collect(Collectors.toList());
  }

  public List<MilitaryPlane> getBomberMilitaryPlanes() {
    return getMilitaryPlanes().stream()
        .filter(plane -> (BOMBER == plane.getRole())).collect(Collectors.toList());
  }

  public void sortByMaxDistance() {
    planes.sort(Comparator.comparingInt(Plane::getMaxFlightDistance));
  }

  public void sortByMaxSpeed() {
    planes.sort(Comparator.comparingInt(Plane::getMaxSpeed));
  }

  public void sortByMaxLoadCapacity() {
    planes.sort(Comparator.comparingInt(Plane::getMaxLoadCapacity));
  }

  private void print(Collection<? extends Plane> collection) {
    for (Plane plane : collection) {
      System.out.println(plane);
    }
  }

  private <T> List<T> getPlanesOfSpecifiedType(Class<T> planeClass){
    List<T> allPlanesOfType = new ArrayList<T>();
    for (Plane plane : planes) {
      if (plane.getClass() == planeClass) {
        allPlanesOfType.add((T) plane);
      }
    }
    return allPlanesOfType;
  }

  @Override
  public String toString() {
    return "Airport{" +
        "Planes=" + planes.toString() +
        '}';
  }
}