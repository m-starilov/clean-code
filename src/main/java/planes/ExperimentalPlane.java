package planes;

import java.util.Objects;
import planes.specifications.Secrecy;
import planes.specifications.Role;

public class ExperimentalPlane extends Plane{

    private Role role;
    private Secrecy secrecy;

    public ExperimentalPlane(String model, int maxSpeed, int maxFlightDistance, int maxLoadCapacity, Role role, Secrecy secrecy) {
        super(model, maxSpeed, maxFlightDistance, maxLoadCapacity);
        this.role = role;
        this.secrecy = secrecy;
    }

    public Secrecy getClassificationLevel(){
        return secrecy;
    }

    public void setClassificationLevel(Secrecy secrecy){
        this.secrecy = secrecy;
    }

    @Override
    public String toString() {
        return super.toString().replace("}",
                "role=" + role +
                ", secrecy=" + secrecy +
                        "} ");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ExperimentalPlane that = (ExperimentalPlane) o;
        return role == that.role && secrecy == that.secrecy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), role, secrecy);
    }
}