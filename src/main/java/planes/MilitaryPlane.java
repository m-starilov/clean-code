package planes;

import planes.specifications.Role;

public class MilitaryPlane extends Plane{

    private final Role role;

    public MilitaryPlane(String model, int maxSpeed, int maxFlightDistance, int maxLoadCapacity, Role role) {
        super(model, maxSpeed, maxFlightDistance, maxLoadCapacity);
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return super.toString().replace("}",
                ", role=" + role +
                '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MilitaryPlane)) return false;
        if (!super.equals(o)) return false;
        MilitaryPlane that = (MilitaryPlane) o;
        return role == that.role;
    }
}