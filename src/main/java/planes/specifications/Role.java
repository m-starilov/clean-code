package planes.specifications;

public enum Role { LIFTING_BODY, HYPERSONIC, HIGH_ALTITUDE, VTOL, FIGHTER, BOMBER, TRANSPORT }