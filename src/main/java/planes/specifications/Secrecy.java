package planes.specifications;

public enum Secrecy { UNCLASSIFIED, CONFIDENTIAL, SECRET, TOP_SECRET }